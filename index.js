const express = require('express');
const app = express();
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const cors = require('cors');
const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const users = require('./app/users');
const track_history = require('./app/track_history');

app.use(express.json());
app.use(cors());
app.use(express.static('public'));
app.use('/artists', artists);
app.use('/albums', albums)
app.use('/tracks', tracks);
app.use('/users', users);
app.use('/track_history', track_history);

const port = 8000;

const run = async () => {
  await mongoose.connect('mongodb://localhost/music-app');

  app.listen(port, () => {
     console.log(`Server started on ${port} port!`);
  });

    exitHook(() => {
        console.log('exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));