const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },

    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album'
    },

    duration: String,
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;