const express = require('express');
const Album = require('../models/Album');
const {nanoid} = require('nanoid');
const multer = require('multer');
const path = require('path');
const config = require("../config");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const router = express.Router();

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const artistAlbums = {};

        if (req.query.artist) {
            artistAlbums.artist = req.query.artist;
            const albums = await Album.find(artistAlbums)
            return res.send(albums);
        }

        const albums = await Album.find();
        res.send(albums);

    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Album.findById(req.params.id).populate('artist', 'name information image');

        if (album) {
            res.send(album);
        } else {
            res.status(404).send({error: 'Product not found'});
        }

    } catch {
        res.status(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.name || !req.body.artist) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const albumData = {
        name: req.body.name,
        year: req.body.year || null,
        artist: req.body.artist,
    };

    if (req.file) {
        albumData.image = req.file.filename;
    }

    const album = new Album(albumData);

    try {
        await album.save();
        res.send(albumData);
    } catch (e) {
        res.status(400).send({error: 'Data not valid'});
    }

});

module.exports = router;