const mongoose = require('mongoose');

const TrackHistorySchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },

    trackId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Track',
        required: true,
    },

    datetime: {
        type: Date,
        default: Date.now(),
    }
});

const TrackHistory = mongoose.model('Track History', TrackHistorySchema);

module.exports = TrackHistory;