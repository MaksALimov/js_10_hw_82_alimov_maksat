const express = require('express');
const Artist = require('../models/Artist');
const config = require('../config');
const {nanoid} = require('nanoid');
const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const router = express.Router();

const upload = multer({storage});

router.get('/', async (req, res) => {
    try {
        const artists = await Artist.find();
        res.send(artists);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.name) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const artistData = {
        name: req.body.name,
        information: req.body.information || null,
    };

    if (req.file) {
        artistData.image = req.file.filename;
    }

    const artist = new Artist(artistData);

    try {
        await artist.save();
        res.send(artist);
    } catch {
        res.status(400).send({error: 'Data not valid'});
    }
});

module.exports = router;