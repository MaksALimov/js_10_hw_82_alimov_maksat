const express = require('express');
const TrackHistory =  require('../models/TrackHistory');
const User = require('../models/User');
const Track = require('../models/Track');
const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Authorization');

    if (!token || !req.body.track) {
        return res.status(401).send({error: 'No token or track present'});
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: 'Unauthorized'})
    }

    const track = await Track.findById(req.body.track);

    if (!track) {
        return res.status(404).send({error: 'Track is not found'});
    }

    const trackHistoryData = {
        userId: user._id,
        trackId: req.body.track,
    }

    const trackHistory = new TrackHistory(trackHistoryData);

    try {
        await trackHistory.save();
        res.send(trackHistory);
    } catch {
        res.status(400).send({error: 'Data not valid'});
    }
});

module.exports = router;