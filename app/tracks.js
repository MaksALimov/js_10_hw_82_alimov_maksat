const express = require('express');
const Track = require('../models/Track');
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const albumId = {};

        if (req.query.album) {
            albumId.album = req.query.album;
            const tracksAlbum = await Track.find(albumId).populate('album', 'name');
            return res.send(tracksAlbum);
        }

        const tracks = await Track.find();
        res.send(tracks);
    } catch {
        res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    if (!req.body.name) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const trackData = {
        name: req.body.name,
        album: req.body.album || null,
        duration: req.body.duration || null,
    };

    const track = new Track(trackData);

    try {
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send({error: 'Data not valid'});
    }
});

module.exports = router;